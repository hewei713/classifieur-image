def predict(x) :
    import numpy as np
    import json
    import pickle

    import cv2
    from PIL import Image
    from keras.models import load_model
    from keras.models import model_from_json


   #%%  img_x, img_y = 230, 350

    #Load the pretrained Tokenizer to encode and decode word, and word freq
    # loading
    with open('E:/M2/PPD/classifieur-image/model/'+'images_shuffeled.pickle', 'rb') as handle:
        images_shuffeled_loaded = pickle.load(handle)
    with open('E:/M2/PPD/classifieur-image/model/'+'label_shuffelef.pickle', 'rb') as handle:
        label_shuffelef_loaded = pickle.load(handle)
        
    with open('E:/M2/PPD/classifieur-image/model/model_in_json.json','r') as f:
        model_json = json.load(f)
        model = model_from_json(model_json)
        model.load_weights('E:/M2/PPD/classifieur-image/model/model.h5')
        print("Loaded model from disk")


    image_to_pred = []
    image_to_pred.append(preprocessing(x,(230,350)))
    image_to_pred=np.asarray(image_to_pred)
    image_to_pred = images_shuffeled_loaded[2].reshape(1,230,350,3)
    image_to_pred=np.asarray(image_to_pred)


    predictions = model.predict(image_to_pred)
    result = np.argmax(predictions[0])
    print(result)
    return result
    
def preprocessing(path,size):
    import glob
    images_list=[]
    names=[]
    files_name_tab=[]
    files_name=glob.glob(path+'/*/*/*.png')
    for file in files_name:
        im=cv2.imread(file)
        im=resize(im,size)
        images_list.append(im)
    return images_list