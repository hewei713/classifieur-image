#!/usr/bin/env python
# coding: utf-8
import os
import numpy as np
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import  Input, Dense ,Conv2D, MaxPooling2D , AveragePooling2D, Dropout,Flatten,Activation
from keras.optimizers import sgd,Adam
from keras.losses import categorical_crossentropy
from keras.losses import binary_crossentropy
import glob
import cv2
import itertools
from tqdm import tqdm
from sklearn.utils import shuffle
from keras.applications import MobileNet
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from keras.optimizers import Adam
from skimage.transform import rescale, resize, downscale_local_mean
from keras.layers.normalization import BatchNormalization
from sklearn.utils import shuffle
import matplotlib.pyplot as plot
import pickle
import json
from keras.models import load_model
from keras.models import model_from_json
import pickle
import matplotlib.pyplot as plot
import random

#Load the pretrained Tokenizer to encode and decode word, and word freq
# loading
with open('E:/M2/PPD/classifieur-image/model/'+'images_shuffeled.pickle', 'rb') as handle:
    images_shuffeled = pickle.load(handle)
with open('E:/M2/PPD/classifieur-image/model/'+'label_shuffelef.pickle', 'rb') as handle:
    label_shuffelef = pickle.load(handle)

x_train,x_test,y_train,y_test=train_test_split(images_shuffeled,label_shuffelef,test_size=0.20)

x_val = x_train[0:100]
y_val = y_train[0:100]
x_train = x_train[100:]
y_train = y_train[100:]

model = Sequential()
model.add(Conv2D(8, kernel_size=(3, 3),activation='relu',input_shape=(230,350,3)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.5))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dropout(0.25))
model.add(Dense(2, activation = 'softmax'))
model.summary()

#%%
model.compile(loss=categorical_crossentropy, optimizer='rmsprop', metrics=['accuracy'])
model.fit(x_train, y_train,
          batch_size=128,
          epochs=15,
          verbose=1,
          validation_data=(x_val, y_val))


scores = model.evaluate(x_test, y_test, verbose=0)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

# lets assume `model` is main model
model_json = model.to_json()
with open("E:/M2/PPD/classifieur-image/model/model_in_json.json", "w") as json_file:
    json.dump(model_json, json_file)
# serialize weights to HDF5
model.save_weights("E:/M2/PPD/classifieur-image/model/model.h5")
print("Saved model to disk")



