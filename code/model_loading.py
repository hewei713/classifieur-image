#!/usr/bin/env python
# coding: utf-8

import os
import numpy as np
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import  Input, Dense ,Conv2D, MaxPooling2D , AveragePooling2D, Dropout,Flatten,Activation
from keras.optimizers import sgd,Adam
from keras.losses import categorical_crossentropy
from keras.losses import binary_crossentropy
import glob
import cv2
import itertools
from tqdm import tqdm
from sklearn.utils import shuffle
from keras.applications import MobileNet
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from keras.optimizers import Adam
from skimage.transform import rescale, resize, downscale_local_mean
from keras.layers.normalization import BatchNormalization
from sklearn.utils import shuffle
import matplotlib.pyplot as plot
import pickle
import json
from keras.models import load_model
from keras.models import model_from_json
import pickle
import matplotlib.pyplot as plot
import random


#Load the pretrained Tokenizer to encode and decode word, and word freq
# loading
with open('E:/M2/PPD/classifieur-image/model/'+'images_shuffeled.pickle', 'rb') as handle:
    images_shuffeled = pickle.load(handle)
with open('E:/M2/PPD/classifieur-image/model/'+'label_shuffelef.pickle', 'rb') as handle:
    label_shuffelef = pickle.load(handle)

x_train,x_test,y_train,y_test=train_test_split(images_shuffeled,label_shuffelef,test_size=0.20)

with open('model_in_json.json','r') as f:
    model_json = json.load(f)

model = model_from_json(model_json)
model.load_weights('E:/M2/PPD/classifieur-image/model/model.h5')
print("Loaded model from disk")

model.compile(loss=categorical_crossentropy, optimizer='rmsprop', metrics=['accuracy'])
scores = model.evaluate(x_test, y_test, verbose=0)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))


