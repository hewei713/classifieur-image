#!/usr/bin/env python
# coding: utf-8
import os
import numpy as np
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import  Input, Dense ,Conv2D, MaxPooling2D , AveragePooling2D, Dropout,Flatten,Activation
from keras.optimizers import sgd,Adam
from keras.losses import categorical_crossentropy
from keras.losses import binary_crossentropy
import glob
import cv2
import itertools
from tqdm import tqdm
from sklearn.utils import shuffle
from keras.applications import MobileNet
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from keras.optimizers import Adam
from skimage.transform import rescale, resize, downscale_local_mean
from keras.layers.normalization import BatchNormalization
from sklearn.utils import shuffle
import matplotlib.pyplot as plot
import pickle
import json
from keras.models import load_model
from keras.models import model_from_json
import pickle
import matplotlib.pyplot as plot
import random
#%%
#Variable globale
#le chemin a modifier selon PC
PATH_A_MODIFIER = 'E:/M2/PPD'
#Deux categories de maladies
cible1='benign'
cible2='malignant'

#Types de maladie
type_benign=['adenosis','fibroadenoma','phyllodes_tumor']
type_maligant=['lobular_carcinoma','mucinous_carcinoma','papillary_carcinoma']

#Fonction de construction chemin d'acces
def buil_path(cible,types):
    paths=[]
    for t in types:
        p= PATH_A_MODIFIER+ "/classifieur-image/Datas/"+cible +"/SOB/" +t
        paths.append(p)
    return paths
#appel de fonction
paths_benign=buil_path(cible1,type_benign)
paths_maligant=buil_path(cible2,type_maligant)

print(paths_maligant)
#%%
#Fonction de traitement (resize de limage) Remarque: une image defaillante SOB_B_PT-14-22704-100-039 a supprimer
def preprocessing(path,size):
    images_list=[]
    names=[]
    files_name_tab=[]
    files_name=glob.glob(path+'/*/*/*.png')
    for file in files_name:
        im=cv2.imread(file)
        im=resize(im,size)
        images_list.append(im)
    return images_list

#%%
#loading data benign
images_benign=[]
#tqdm barre d'avancement
for path in tqdm(paths_benign):
    #ajout dans la liste images_begnin : append
    images_benign.append(preprocessing(path,(230,350)))

images_benign = list(itertools.chain(*images_benign))


#%%
#loading data maligant
images_maligant=[]
for path in tqdm(paths_maligant):
    images_maligant.append(preprocessing(path,(230,350)))

images_maligant = list(itertools.chain(*images_maligant))

#%%
#labelisation 0 pour benign et 1 pour malingnant
y_benign=np.zeros((len(images_benign),))
y_malignant=np.ones((len(images_maligant),))

#%%
y=np.concatenate([y_benign,y_malignant])
label=to_categorical(y)
#%%
images=images_benign+images_maligant
#%%
#normalisation
images=np.asarray(images)

#%%
#%%
#Melange du dataset avant de le spliter
images_shuffeled, label_shuffelef= shuffle(images,label)

# saving the model
with open('E:/M2/PPD/classifieur-image/model/'+'images_shuffeled.pickle', 'wb') as handle:
    pickle.dump(images_shuffeled, handle, protocol=pickle.HIGHEST_PROTOCOL)
with open('E:/M2/PPD/classifieur-image/model/'+'label_shuffelef.pickle', 'wb') as handle:
    pickle.dump(label_shuffelef, handle, protocol=pickle.HIGHEST_PROTOCOL)
