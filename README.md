Projet PPD : Classifieur Image

## Description

Le projet consiste à développer un réseau de neurones assez profond pour être capable d’apprendre les différentes caractéristiques qui permettent de distinguer une image de cellules tumorales d’une image de cellules saines à partir de l’expérience de plusieurs médecins. 
Ce réseau sera ensuite être exploité par les équipes médicales pour identifier automatiquement un patient malade.


## Installation

Mise en place : 
* Cloner le projet sur le dépot GIT,
* Dans le terminal, se positionner sur le fichier de l'application
Lancer dans l'ordre pour entrainer le modèle : 
* dataset_loading.py
* model_training.py
* model_loading.py

Puis lancer l'application Shiny

